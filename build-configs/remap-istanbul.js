var loadCoverage = require('remap-istanbul/lib/loadCoverage');
var remap = require('remap-istanbul/lib/remap');
var writeReport = require('remap-istanbul/lib/writeReport');

var coverage_before = 'dist\\tests\\code-coverage-lcov\\PhantomJS 1.9.8 (Windows 7 0.0.0)\\coverage-final.json';
var lcov_after = 'dist\\tests\\code-coverage-lcov\\PhantomJS 1.9.8 (Windows 7 0.0.0)\\lcov_final.info';
var coverage = loadCoverage(coverage_before);
var collector = remap(coverage, {
    token : "typescript\\",
    sourceMapsDirs : "\\dist\\"
});
writeReport(collector, 'lcovonly', lcov_after).then(function () {
    /* do something else now */
});

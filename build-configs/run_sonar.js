/*
 * TAKEN FROM :
 *
 * grunt-sonar-runner
 * https://github.com/skhatri/grunt-sonar-runner
 *
 * Copyright (c) 2014 Suresh Khatri
 * Licensed under the MIT license.
 */

'use strict';

var project = require('../project');

// Project config
var projectKey = project.id.name;
var projectName = project.id.name;
var projectVersion = project.id.version;
var tslintconfigpath = project.layout.tslintconfigpath;
var reportpath = project.layout.lcovReportPath;
var sources = [project.layout.frontEndSourcesRoot].join(',');

// Sonar config

var sonarRunnerVersion = "sonar-runner-2.4";
var sonarDistJarAbsolutaPathFromSONAR_RUNNER_HOME = '/lib/sonar-runner-dist-2.4.jar';
var sonarHostUrl = project.sonar.host.url || 'http://localhost:9000';

// Script

var childProcess = require('child_process'),
    format = require('util').format,
    os = require('os'),
    fs = require('fs');

var SONAR_RUNNER_HOME = process.env.SONAR_RUNNER_HOME || __dirname + '/../' + sonarRunnerVersion;
var SONAR_RUNNER_OPTS = process.env.SONAR_RUNNER_OPTS || "";

var JAR = sonarDistJarAbsolutaPathFromSONAR_RUNNER_HOME;
var SONAR_RUNNER_COMMAND = 'java ' + SONAR_RUNNER_OPTS + ' -jar ' + SONAR_RUNNER_HOME + JAR + ' -Drunner.home=' + SONAR_RUNNER_HOME;
var LIST_CMD = (/^win/).test(os.platform()) ? 'dir ' + SONAR_RUNNER_HOME + JAR : 'ls ' + SONAR_RUNNER_HOME + JAR;

var mergeOptions = function (prefix, effectiveOptions, obj) {
    for (var j in obj) {
        if (obj.hasOwnProperty(j)) {

            if (typeof obj[j] === 'object') {
                mergeOptions(prefix + j + '.', effectiveOptions, obj[j]);
            } else {
                effectiveOptions[prefix + j] = obj[j];
            }
        }
    }
};

doIt();

function doIt() {

    var options = {
        debug: true,
        separator: '\n',
        sonar: {
            host: {
                url: sonarHostUrl
            }/*,
             jdbc: {
             url: 'jdbc:mysql://localhost:3306/sonar',
             username: 'sonar',
             password: 'sonar'
             }*/,

            projectKey: projectKey,
            projectName: projectName,
            projectVersion: projectVersion,
            sources: sources,
            language: 'ts',
            sourceEncoding: 'UTF-8',
            ts: {
                tslintconfigpath: tslintconfigpath,
                lcov: {
                    reportpath: reportpath
                }
            }
        }
    };


    var callback = function () {};
    var dryRun = options.dryRun;


    // in case the language property isn't set sonar assumes this is a multi language project
    if (options.sonar.language) {
        options.sonar.language = options.sonar.language;
    }
    options.sonar.sourceEncoding = options.sonar.sourceEncoding || 'UTF-8';
    options.sonar.host = options.sonar.host || {url: 'http://localhost:9000'};
    if (Array.isArray(options.sonar.exclusions)) {
        options.sonar.exclusions = options.sonar.exclusions.join(',');
    }

    var effectiveOptions = Object.create(null);
    mergeOptions('sonar.', effectiveOptions, options.sonar);

    var props = [];
    if (options.debug) {
        console.info('Effective Sonar Options');
        console.info('-----------------------');
    }
    for (var o in effectiveOptions) {
        var line = o + '=' + effectiveOptions[o];
        props.push(line);
        if (options.debug) {
            console.info(line);
        }
    }

    fs.writeFile(SONAR_RUNNER_HOME + '/conf/sonar-runner.properties', props.join(options.separator), function (err) {
        if (err) {
            return console.log(err);
        }

        console.log("The file was saved!");
        if (options.debug) {
            console.info('Sonar client configured ');
        }


        var execCmd = dryRun ? LIST_CMD : SONAR_RUNNER_COMMAND;

        console.info("sonar-runner exec: " + SONAR_RUNNER_COMMAND);

        var exec = childProcess.exec(execCmd,
            options.maxBuffer ? {maxBuffer: options.maxBuffer} : {},
            callback);

        exec.stdout.on('data', function (c) {
            console.info(c);
        });
        exec.stderr.on('data', function (c) {
            console.error(c);
        });

        exec.on('exit', function (code) {
            if (code !== 0) {
                console.error(format('Return code: %d.', code));
                return false;
            }
            console.log(format('Return code: %d.', code));
            return true;
        });

    });

};



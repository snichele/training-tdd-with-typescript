/*
 * Webpack config file that drives the 'package' npm task.
 */

var project = require('../project');

var projectLayout = project.layout;
var applications = project.applications;

module.exports = applications.map(function (next) {
    return {
        entry: next.entry,
        output: {
            path: projectLayout.buildTo,
            filename: next.jsBundleFileName,
            library: next.library,
            libraryTarget: next.libraryTarget
        },
        devtool: 'source-map',
        plugins: [],
        resolve: {
            extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
        },
        module: {
            preLoaders: [
                {
                    test: /\.ts$/,
                    loader: "tslint?configFilePath=tslint.json"
                }
            ],
            loaders: [
                {
                    test: /\.ts$/,
                    loader: 'awesome-typescript-loader?tsconfig=' + projectLayout.frontEndSourcesRoot + '/tsconfig.json&resolveGlobs=false'
                }
            ]
        }, tslint: {
            // tslint errors are displayed by default as warnings
            // set emitErrors to true to display them as errors
            emitErrors: true,

            // tslint does not interrupt the compilation by default
            // if you want any file with tslint errors to fail
            // set failOnHint to true
            failOnHint: true,

            // name of your formatter (optional)
            //formatter: "yourformatter",

            // path to directory contating formatter (optional)
            formattersDirectory: "node_modules/tslint-loader/formatters/"
        }
    };
});

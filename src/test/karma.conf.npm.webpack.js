/*
 * TODO : cleanse all hard wired things (path...) in there !
 */

var webpack = require("webpack");
var testDir = "./src/test/typescript/";
var basePath = '../';

var files = walkSync(testDir, "-spec.ts").map(function (f) {
    return basePath + testDir + f;
});
console.info(files);

var preprocessors = {
    '../src/main/typescript/**/*.ts': ['webpack'],
    '../src/test/typescript/**/*-spec.ts': ['webpack']
};

module.exports = function (config) {
    config.set({
        basePath: basePath,
        frameworks: ['jasmine'],
        files: files,
        exclude: [],
        preprocessors: preprocessors,
        reporters: ['spec', 'coverage'],
        port: 9876,
        colors: true,
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: ['PhantomJS2'],
        singleRun: true,
        coverageReporter: {
            reporters: [{
                type: 'lcov',
                dir: '../dist/tests/code-coverage-lcov/'
            }, {
                type: 'json',
                dir: '../dist/tests/code-coverage-lcov/'
            }, {
                type: 'text'
            }]
        },
        webpack: {
            name: "training-tdd-with-typescript",
            resolve: {
                extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
            },
            module: {
                loaders: [
                    {
                        test: /\.ts$/,
                        loader: 'awesome-typescript-loader'
                    }
                ], postLoaders: [{
                    test: /\.ts$/,
                    exclude: /.*-spec\.ts$/gmi,
                    loader: 'istanbul-instrumenter'
                }]
            },
            devtool: 'inline-source-map'
        },
        webpackMiddleware: {
            // webpack-dev-middleware configuration
            // i. e.
            // noInfo: true
        }, plugins: [
            require("karma-jasmine"),
            require("karma-coverage"),
            require("karma-spec-reporter"),
            require("karma-phantomjs2-launcher"),
            require("karma-webpack")
        ]
    });
};

function walkSync(dir, takeIfEndsWith, filelist) {
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(dir + file).isDirectory()) {
            filelist = walkSync(dir + file + '/', takeIfEndsWith, filelist);
        }
        else {
            if (file.indexOf(takeIfEndsWith) !== -1) {
                filelist.push(file);
            }
        }
    });
    return filelist;
}

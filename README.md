# training-tdd-with-typescript

A repo to illustrate some progressive TDD resolution of various exercises in TypeScript.

Look at 'branches' to explore the various 'exercises' (beware, some branches may be experimental fork of others).

Most branches can run tests with `npm run test` after an `npm install`.

Some branches support code coverage measuring into a local sonar instance on port 9000 with default h2 database with
`npm run sonar`.

## Disclaimer

This is a public repository only for sharing convenience at work ; this is not intended to reflect any opinion, state of the art demonstration or anything else. **Use with caution !**

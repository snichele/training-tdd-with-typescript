/*
 Project configuration (like a 'pom')
 */
var frontEndSourcesRoot = './src/main/typescript/';

var projectName = 'training-tdd-with-typescript';

module.exports = {
    id: {
        name: projectName,
        groupId: projectName,
        version: "0.0.1"
    },
    layout: {
        buildTo: 'dist/',
        frontEndSourcesRoot: frontEndSourcesRoot,
        frontEndTestSourcesRoot: './src/test/typescript/',
        tslintconfigpath: './tslint.json',
        lcovReportPath: './dist/tests/code-coverage-lcov/PhantomJS 1.9.8 (Windows 7 0.0.0)/lcov_final.info'
    },
    applications: [{
        name: projectName,
        jsBundleFileName: projectName + '.js',
        entry: [frontEndSourcesRoot + "/" + projectName + ".ts"],
        library: projectName,
        libraryTarget: "umd"
    }],
    sonar: {
        host: {
            url: 'http://localhost:9000'
        }
    }
};
